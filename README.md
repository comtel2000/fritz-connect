fritz-connect (SwitcherApp)
===========================

[![Build Status](https://travis-ci.org/comtel2000/fritz-connect.png)](https://travis-ci.org/comtel2000/fritz-connect)

JavaFX 8 FRITZ!Box smart home actor switcher app
- use webservice of AVM router (FRITZ!OS 5.55 or later)
- thx to ControlsFX and oracle sample CSS goodies

![Splash] (doc/app_screen.png)